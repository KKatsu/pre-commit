terraform {
    source = "git::ssh://git@gitlab.com/../modulos.git//aws/ec2"
}

include {
    path = find_in_parent_folders()
}

locals {
    account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))
}

inputs = {

    #Tags
    tags = {
        Squad = "sre"
    }
    environment = local.account_vars.locals.environment

    #EC2
    number_of_instances    = ["instance-1", "instance-2", "instance-3"]
    instance_type          = "t2.micro"
    vpc_security_group_ids = ["sg-00000000"]
    subnet_id              = "subnet-eddcdzz4"

}
