# ------------------------------------------------------------------------------
# TAGS
# ------------------------------------------------------------------------------

module tags {
  source = "../tags"

  environment = var.environment
  tags        = var.tags
}

# ------------------------------------------------------------------------------
# S3
# ------------------------------------------------------------------------------

module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = var.bucket
  acl    = var.acl

  versioning = {
    enabled = true
  }

  tags = module.tags.tags
}
