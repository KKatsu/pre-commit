terraform {
    source = "git::ssh://git@gitlab.com/../modulos.git//aws/s3"
}

include {
    path = find_in_parent_folders()
}

locals {
    account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))
}

inputs = {

    #Tags
    tags = {
        Squad = "sre"
    }
    environment = local.account_vars.locals.environment

    #S3
    bucket = "name-for-bucket"
    acl    = "private"
}
